###################################### ICS HWI ###############################################
##  CCDB device types: ICS_xxxxx                                                            ## 
##                                                                                          ##  
##                                     CMS Monitoring Function                              ##
##                                                                                          ##  
##                                                                                          ## 
############################         Version: 2.0             ################################
# Author:  Attila Horvath
# Date:    26-06-2023
# Version: v2.0
# Changes: 
# 1. PVs added and renamed  
############################         Version: 1.1             ################################
# Author:  Attila Horvath
# Date:    08-08-2022
# Version: v1.1
# Changes: 
# 1. Archived PVs updated    
############################ Version: 1.0             ########################################
# Author:  Miklos Boros 
# Date:    06-05-2020
# Version: v1.0



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Step Messages
add_string("StrMsg1", 39,         PV_NAME="StrMsg1",            PV_DESC="Step Message 1")
add_string("StrMsg2", 39,         PV_NAME="StrMsg2",            PV_DESC="Step Message 2")


#Status BITs
add_digital("STS_Inactive",  		ARCHIVE=True,         		PV_DESC="Monitoring Function Inactive",               	PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_Active",  			ARCHIVE=True,           	PV_DESC="Monitoring Function Active",                 	PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_GroupFActive",  	ARCHIVE=True,     			PV_DESC="Failure Action Active",           				PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_Operator",  		ARCHIVE=True,         		PV_DESC="Operator Action",            					PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_ParametersOK",  	ARCHIVE=True,         		PV_DESC="Parameters OK",	            				PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_HH_LvlReached",  	ARCHIVE=True,         		PV_DESC="HiHi Level Reached",	            			PV_ONAM="True",                       PV_ZNAM="False")
add_digital("STS_Timeout",  		ARCHIVE=True,         		PV_DESC="Step Timeout",	            					PV_ONAM="True",                       PV_ZNAM="False")

#Initial condition
add_analog("STS_ActStep","INT",  	ARCHIVE=True,               PV_DESC="Step Actual Step ID")

#State dependent Interlock
add_minor_alarm("StateInterlock",     "StateInterlock Active",                                    PV_ZNAM="True")

#Active BlockIcon Buttons
add_digital("BTN_Act_Ena",            PV_DESC="Activate Button Enable",          PV_ONAM="True",                       PV_ZNAM="False")
add_digital("BTN_Act_Disa",           PV_DESC="Activate Button Disable",         PV_ONAM="True",                       PV_ZNAM="False")


#State alarms
add_digital("GroupAlarm",             PV_DESC="Group Alarm",                     PV_ONAM="True",                       PV_ZNAM="False")
add_major_alarm("Alarm1",            "Alarm1",                                                    PV_ZNAM="True")
add_major_alarm("Alarm2",            "Alarm2",                                                    PV_ZNAM="True")
add_major_alarm("Alarm3",            "Alarm3",                                                    PV_ZNAM="True")
add_major_alarm("Alarm4",            "Alarm4",                                                    PV_ZNAM="True")
add_major_alarm("Alarm5",            "Alarm5",                                                    PV_ZNAM="True")

add_digital("GroupWarning",                                                 	PV_DESC="Monitoring Function  Warning",                PV_ONAM="True",                       PV_ZNAM="False")
add_minor_alarm("Warning1",          "Warning1",                                                  PV_ZNAM="True")
add_minor_alarm("Warning2",          "Warning2",                                                  PV_ZNAM="True")
add_minor_alarm("Warning3",          "Warning3",                                                  PV_ZNAM="True")
add_minor_alarm("Warning4",          "Warning4",                                                  PV_ZNAM="True")
add_minor_alarm("Warning5",          "Warning5",                                                  PV_ZNAM="True")
add_minor_alarm("Warning6",          "Warning6",                                                  PV_ZNAM="True")
add_minor_alarm("Warning7",          "Warning7",                                                  PV_ZNAM="True")
add_minor_alarm("Warning8",          "Warning8",                                                  PV_ZNAM="True")
add_minor_alarm("Warning9",          "Warning9",                                                  PV_ZNAM="True")
add_minor_alarm("Warning10",         "Warning10",                                                 PV_ZNAM="True")
add_minor_alarm("Warning11",         "Warning11",                                                 PV_ZNAM="True")
add_minor_alarm("Warning12",         "Warning12",                                                 PV_ZNAM="True")
add_minor_alarm("Warning13",         "Warning13",                                                 PV_ZNAM="True")
add_minor_alarm("Warning14",         "Warning14",                                                 PV_ZNAM="True")
add_minor_alarm("Warning15",         "Warning15",                                                 PV_ZNAM="True")

add_digital("OPI_Warning1",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning2",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning3",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning4",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning5",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning6",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning7",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning8",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning9",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OPI_Warning10",                                              		PV_DESC="OPI Warning1",									PV_ONAM="True",                       PV_ZNAM="False")

#Flag bits
add_digital("Flag1",  ARCHIVE=True,                  			PV_DESC="Flag1",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag2",  ARCHIVE=True,                  			PV_DESC="Flag2",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag3",  ARCHIVE=True,                  			PV_DESC="Flag3",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag4",  ARCHIVE=True,                  			PV_DESC="Flag4",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag5",  ARCHIVE=True,                  			PV_DESC="Flag5",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag6",  ARCHIVE=True,                  			PV_DESC="Flag6",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag7",  ARCHIVE=True,                  			PV_DESC="Flag7",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag8",  ARCHIVE=True,                  			PV_DESC="Flag8",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag9",  ARCHIVE=True,                  			PV_DESC="Flag9",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("Flag10", ARCHIVE=True,                 			PV_DESC="Flag10",     PV_ONAM="True",                       PV_ZNAM="False")


#Parameter Feedback
add_analog("FB_S1",            "REAL",                           PV_DESC="Feedback Parameter1")
add_analog("FB_S2",            "REAL",                           PV_DESC="Feedback Parameter2")
add_analog("FB_S3",            "REAL",                           PV_DESC="Feedback Parameter3")
add_analog("FB_S4",            "REAL",                           PV_DESC="Feedback Parameter4")
add_analog("FB_S5",            "REAL",                           PV_DESC="Feedback Parameter5")
add_analog("FB_S6",            "REAL",                           PV_DESC="Feedback Parameter6")
add_analog("FB_S7",            "REAL",                           PV_DESC="Feedback Parameter7")
add_analog("FB_S8",            "REAL",                           PV_DESC="Feedback Parameter8")
add_analog("FB_S9",            "REAL",                           PV_DESC="Feedback Parameter9")
add_analog("FB_S10",           "REAL",                           PV_DESC="Feedback Parameter10")
add_analog("FB_S11",           "REAL",                           PV_DESC="Feedback Parameter11")
add_analog("FB_S12",           "REAL",                           PV_DESC="Feedback Parameter12")
add_analog("FB_S13",           "REAL",                           PV_DESC="Feedback Parameter13")
add_analog("FB_S14",           "REAL",                           PV_DESC="Feedback Parameter14")
add_analog("FB_S15",           "REAL",                           PV_DESC="Feedback Parameter15")
add_analog("FB_S16",           "REAL",                           PV_DESC="Feedback Parameter16")
add_analog("FB_S17",           "REAL",                           PV_DESC="Feedback Parameter17")
add_analog("FB_S18",           "REAL",                           PV_DESC="Feedback Parameter18")
add_analog("FB_S19",           "REAL",                           PV_DESC="Feedback Parameter19")
add_analog("FB_S20",           "REAL",                           PV_DESC="Feedback Parameter20")
add_digital("FB_S1_Constant",                  				     PV_DESC="S1 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S2_Constant",                  				     PV_DESC="S2 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S3_Constant",                  				     PV_DESC="S3 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S4_Constant",                  				     PV_DESC="S4 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S5_Constant",                  				     PV_DESC="S5 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S6_Constant",                  				     PV_DESC="S6 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S7_Constant",                  				     PV_DESC="S7 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S8_Constant",                  				     PV_DESC="S8 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S9_Constant",                  				     PV_DESC="S9 Value is Constant",      PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S10_Constant",                  				 PV_DESC="S10 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S11_Constant",                  				 PV_DESC="S11 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S12_Constant",                  				 PV_DESC="S12 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S13_Constant",                  				 PV_DESC="S13 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S14_Constant",                  				 PV_DESC="S14 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S15_Constant",                  				 PV_DESC="S15 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S16_Constant",                  				 PV_DESC="S16 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S17_Constant",                  				 PV_DESC="S17 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S18_Constant",                  				 PV_DESC="S18 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S19_Constant",                  				 PV_DESC="S19 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")
add_digital("FB_S20_Constant",                  				 PV_DESC="S20 Value is Constant",     PV_ONAM="True",                       PV_ZNAM="False")



add_digital("BTN_YES_ENA",            	PV_DESC="BTN_YES_ENA",     		PV_ONAM="True",             PV_ZNAM="False")
add_digital("BTN_OK_ENA",             	PV_DESC="BTN_OK_ENA",      		PV_ONAM="True",             PV_ZNAM="False")
add_digital("BTN_NO_ENA",            	PV_DESC="BTN_NO_ENA",      		PV_ONAM="True",             PV_ZNAM="False")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("CMD_Activate",				PV_DESC="CMD_Activate",        	PV_ONAM="True",				PV_ZNAM="False")
add_digital("CMD_DeActivate",			PV_DESC="CMD_DeActivate",      	PV_ONAM="True",             PV_ZNAM="False")

add_digital("CMD_YES",					PV_DESC="CMD_YES",    			PV_ONAM="True",             PV_ZNAM="False")
add_digital("CMD_OK",					PV_DESC="CMD_OK",     			PV_ONAM="True",             PV_ZNAM="False")
add_digital("CMD_NO",					PV_DESC="CMD_NO",     			PV_ONAM="True",             PV_ZNAM="False")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

add_analog("P_S1",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter1")
add_analog("P_S2",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter2")
add_analog("P_S3",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter3")
add_analog("P_S4",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter4")
add_analog("P_S5",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter5")
add_analog("P_S6",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter6")
add_analog("P_S7",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter7")
add_analog("P_S8",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter8")
add_analog("P_S9",            "REAL",  ARCHIVE=True,                           PV_DESC="Parameter9")
add_analog("P_S10",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter10")
add_analog("P_S11",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter11")
add_analog("P_S12",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter12")
add_analog("P_S13",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter13")
add_analog("P_S14",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter14")
add_analog("P_S15",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter15")
add_analog("P_S16",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter16")
add_analog("P_S17",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter17")
add_analog("P_S18",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter18")
add_analog("P_S19",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter19")
add_analog("P_S20",           "REAL",  ARCHIVE=True,                           PV_DESC="Parameter20")

